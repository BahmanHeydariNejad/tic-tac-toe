package ir.games.tictactoe.logicalModel;

import lombok.Getter;

public enum PieceType {

    PLUS("/plus100x100.jpg"), MINUS("/minus100x100.jpg"), EMPTY("/empty100x100.jpg"), SQUARE("/square100x100.jpg");

    @Getter
    private String path;

    PieceType(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return path;
    }
}