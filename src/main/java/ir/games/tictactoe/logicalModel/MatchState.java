package ir.games.tictactoe.logicalModel;

public enum MatchState {

    START, WAITING, READY, RUNNING, END
}