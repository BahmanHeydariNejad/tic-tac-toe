package ir.games.tictactoe.logicalModel;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
public class Player {

    private String id;

    private String displayName;

    @Setter
    private PieceType pieceType;

    @Setter
    private int point;

    public Player(String id, String displayName, PieceType pieceType) {
        this.id = id;
        this.displayName = displayName;
        this.pieceType = pieceType;
        this.point = 0;
    }

    public void addPoint(int point) {
        this.point += point;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Player)) return false;
        Player player = (Player) o;
        return this.id.equalsIgnoreCase(player.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
