package ir.games.tictactoe.logicalModel;

import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Getter
public class Match {

    private List<Player> players;

    private Player matchWinner;

    private Player turn;

    private MatchState matchState;

    private Board board;
    @Setter
    private int boardSize = 5;

    @Getter
    @Setter
    private PieceType defaultPieceType;

    private Timer turnTime;


    private Integer stepsRemain;


    public Match() {
        this.players = new ArrayList<>();
    }

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    public void start() {
        this.board = new Board(boardSize, defaultPieceType);
        makeTurn();
    }

    public void reset() {
        this.board.reset();
        makeTurn();
    }

    public void move(Movement movement) {
        this.board.movementStep(movement);
        if (checkWin(movement)) {
            matchWinner = movement.getPlayer();
            matchWinner.addPoint(1);
            reset();
            return;
        }
        makeTurn();
    }

    public Player getTotalWinner() {
        return players.stream().sorted(Comparator.comparingInt(Player::getPoint)).findFirst().get();
    }

    private void makeTurn() {
        int index = players.indexOf(turn);
        if (index >= 0) {
            if (++index >= players.size()) {
                index = 0;
            }
            turn = players.get(index);
        } else {
            turn = getRandomTurn();
        }
    }

    private Player getRandomTurn() {
        return players.get(new Random().nextInt(players.size()));
    }

    private boolean checkWin(Movement movement) {
        return this.board.xMatch(movement) || this.board.yMatch(movement) || this.board.lxyMatch(movement) || this.board.rxyMatch(movement);
    }

}
