package ir.games.tictactoe.logicalModel;

import lombok.Getter;
import lombok.Setter;

@Getter
public class Movement {

    private Player player;
    private int columnIndex;

    @Setter
    private int rowIndex;

    public Movement(Player player, int columnIndex) {
        this.player = player;
        this.columnIndex = columnIndex;
    }
}
