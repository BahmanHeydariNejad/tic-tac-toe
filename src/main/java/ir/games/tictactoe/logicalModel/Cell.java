package ir.games.tictactoe.logicalModel;

import lombok.Getter;
import lombok.Setter;

public class Cell {

    @Getter
    private int row;

    @Getter
    private int column;

    @Getter
    private PieceType defaultPieceType;

    @Setter
    @Getter
    private Player player = null;

    public Cell(int row, int column, PieceType defaultPieceType) {
        this.row = row;
        this.column = column;
        this.defaultPieceType = defaultPieceType;
    }

    public boolean isEmpty() {
        return player == null;
    }
}
