package ir.games.tictactoe.logicalModel;

import lombok.Getter;

public class Board {

    private String matchWin = "111";
    @Getter
    private Cell[][] cells;

    @Getter
    private int boardSize;

    @Getter
    private PieceType defaultPieceType;

    public Board(int boardSize, PieceType defaultPieceType) {
        this.boardSize = boardSize;
        this.defaultPieceType = defaultPieceType;
    }

    public void reset() {
        cells = new Cell[boardSize][boardSize];
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                this.cells[i][j] = new Cell(i, j, defaultPieceType);
            }
        }
    }

    public void movementStep(Movement movement) {
        if (cells != null)
            for (int i = 0; i < cells.length; i++)
                if (cells[i][movement.getColumnIndex()].isEmpty()) {
                    cells[i][movement.getColumnIndex()].setPlayer(movement.getPlayer());
                    movement.setRowIndex(i);
                    return;
                }
    }

    public boolean xMatch(Movement movement) {
        String c = "";
        for (int x = 0; x < cells.length && !cells[x][movement.getRowIndex()].isEmpty(); x++) {
            c += movement.getPlayer().equals(cells[x][movement.getRowIndex()].getPlayer()) ? "1" : "0";
        }
        return c.contains(matchWin);
    }

    public boolean yMatch(Movement movement) {
        String c = "";
        for (int y = 0; y < cells.length && !cells[movement.getColumnIndex()][y].isEmpty(); y++) {
            c += movement.getPlayer().equals(cells[movement.getColumnIndex()][y].getPlayer()) ? "1" : "0";
        }
        return c.contains(matchWin);
    }

    public boolean lxyMatch(Movement movement) {
        int x = movement.getColumnIndex();
        int y = movement.getRowIndex();
        while (y != 0) {
            x--;
            y--;
        }
        String c = "";
        for (; !cells[x][y].isEmpty(); x--, y++) {
            c += movement.getPlayer().equals(cells[x][y].getPlayer()) ? "1" : "0";
        }
        return c.contains(matchWin);
    }

    public boolean rxyMatch(Movement movement) {
        int x = movement.getColumnIndex();
        int y = movement.getRowIndex();
        while (y != 0) {
            x--;
            y--;
        }
        String c = "";
        for (; !cells[x][y].isEmpty(); x++, y++) {
            c += movement.getPlayer().equals(cells[x][y].getPlayer()) ? "1" : "0";
        }
        return c.contains(matchWin);
    }

    public Player whichIsHere(int row, int column) {
        if (cells != null && cells[row][column] != null)
            return cells[row][column].getPlayer();
        return null;
    }

}
