package ir.games.tictactoe;

import ir.games.tictactoe.visualModel.BaseModule;
import ir.games.tictactoe.visualModel.actions.ActionHandler;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@DependsOn("gameContext")
public class ActionManager implements BaseModule {

    private Map<Class, ActionHandler> actionHandlers;

    public ActionManager(List<ActionHandler> actionHandlers) {
        this.actionHandlers = new HashMap<>();
        actionHandlers.stream().forEach(actionHandler -> {
            this.actionHandlers.put(actionHandler.getClass(), actionHandler);
        });
    }

    @PostConstruct
    @Override
    public void init() {
        actionHandlers.entrySet().stream().forEach(actionHandler -> actionHandler.getValue().init());
    }

    public ActionHandler getActionHandler(Class aClass) {
        return actionHandlers.get(aClass);
    }

}
