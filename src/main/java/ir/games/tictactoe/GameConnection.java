package ir.games.tictactoe;


import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class GameConnection {

    private static final Logger LOGGER = LoggerFactory.getLogger(GameConnection.class);

    private GameEncryptor gameEncryptor;

    private ServerSocket serverSocket;

    @Getter
    private Socket server;

    @Getter
    private Socket client;

    @Async
    public void startServer() throws IOException {
        LOGGER.trace("Server starting ...");
        serverSocket = new ServerSocket(0);
        // Waiting time for opponent response
        serverSocket.setSoTimeout(30000);
        LOGGER.trace("Waiting for client connection on port {}", serverSocket.getLocalPort());
        server = serverSocket.accept();//establishes connection
        LOGGER.trace("Client connected.");
        DataInputStream dis = new DataInputStream(server.getInputStream());
        String str = dis.readUTF();
        LOGGER.trace("message= " + str);
        serverSocket.close();
    }

    @Async
    public void connectToServer(String address, int port) throws IOException {
        LOGGER.trace("Client starting ...");
        client = new Socket(address, port);
        LOGGER.trace("Connected to server.");
        DataOutputStream dout = new DataOutputStream(client.getOutputStream());
        dout.writeUTF("Hello Server");
        LOGGER.trace("Message sent.");
        dout.flush();
//        dout.close();
//        client.close();
    }
}
