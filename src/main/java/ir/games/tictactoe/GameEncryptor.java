package ir.games.tictactoe;

//import com.sun.xml.internal.messaging.saaj.packaging.mime.util.BASE64DecoderStream;
//import com.sun.xml.internal.messaging.saaj.packaging.mime.util.BASE64EncoderStream;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Getter
@Setter
@Component
public class GameEncryptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(GameEncryptor.class);

    private Cipher ecipher;
    private Cipher dcipher;

    private SecretKey key;

    public GameEncryptor() {
        try {
            // generate secret key using DES algorithm
            this.key = KeyGenerator.getInstance("DES").generateKey();
            LOGGER.trace("Key: {}", this.key);

            ecipher = Cipher.getInstance("DES");
            dcipher = Cipher.getInstance("DES");

            // initialize the ciphers with the given key
            ecipher.init(Cipher.ENCRYPT_MODE, this.key);
            dcipher.init(Cipher.DECRYPT_MODE, this.key);

            String testMessage = "This is a classified message!";
            LOGGER.trace("Initial Message: {}", testMessage);
            String encrypted = encrypt(testMessage);
            LOGGER.trace("Encrypted: ", encrypted);
            String decrypted = decrypt(encrypted);
            LOGGER.trace("Decrypted: {}", decrypted);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            LOGGER.error("{}: {}", e.getClass(), e.getMessage());
        }

    }

    public String decrypt(String str) {
        try {
            // decode with base64 to get bytes
//            byte[] dec = BASE64DecoderStream.decode(str.getBytes());
//            byte[] utf8 = dcipher.doFinal(dec);

            // create new string based on the specified charset
//            return new String(utf8, StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String encrypt(String str) {
        try {
            // encode the string into a sequence of bytes using the named charset

            // storing the result into a new byte array.
            byte[] utf8 = str.getBytes(StandardCharsets.UTF_8);
            byte[] enc = ecipher.doFinal(utf8);

            // encode to base64
//            enc = BASE64EncoderStream.encode(enc);
            return new String(enc);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
