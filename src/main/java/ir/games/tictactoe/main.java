package ir.games.tictactoe;

public class main {

    public static void main(String[] args) {

        String url = String.format("%s/otp=%s&traceCode=%s", "A", "B", "C");
        System.out.println(url);

        System.out.println(calculateBankLoadShare(130, 0.18, 24));
        System.out.println(calculateBankLoadShare(130, 0.18, 36));
        System.out.println(calculateBankLoadShare(130, 0.18, 48));
        System.out.println(calculateBankLoadShare(130, 0.18, 60));

    }

    private static double calculateBankLoadShare(double loan, double yearlyInterest, int returnPeriodInMonth) {
        double monthlyInterest = yearlyInterest / 12;
        return (loan + (monthlyInterest * returnPeriodInMonth * loan)) / returnPeriodInMonth;
    }
}