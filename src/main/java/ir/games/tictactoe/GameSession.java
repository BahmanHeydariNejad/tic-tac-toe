package ir.games.tictactoe;

import ir.games.tictactoe.logicalModel.Match;
import ir.games.tictactoe.logicalModel.PieceType;
import ir.games.tictactoe.logicalModel.Player;
import ir.games.tictactoe.visualModel.BaseModule;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Getter
@Setter
@Component
public class GameSession implements BaseModule {

    private static final Logger logger = LoggerFactory.getLogger(GameSession.class);

    private GameConnection gameConnection;

    private Match match;

    @PostConstruct
    @Override
    public void init() {
        match = new Match();
        match.setBoardSize(10);
        match.addPlayer(new Player("1", "Milad", PieceType.MINUS));
        match.addPlayer(new Player("2", "Bahman", PieceType.PLUS));
        match.addPlayer(new Player("3", "Saeed", PieceType.SQUARE));
        match.addPlayer(new Player("4", "Saeed2", PieceType.SQUARE));
        match.addPlayer(new Player("5", "Saeed3", PieceType.SQUARE));
        match.addPlayer(new Player("6", "Saeed4", PieceType.SQUARE));
    }
}
