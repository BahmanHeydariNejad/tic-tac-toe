package ir.games.tictactoe;

import ir.games.tictactoe.visualModel.BaseModule;
import ir.games.tictactoe.visualModel.GameFrame;
import lombok.Getter;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Getter
@Component
public class GameContext implements BaseModule {

    private GameSession gameSession;
    private GameFrame gameFrame;

    public GameContext(GameSession gameSession, GameFrame gameFrame) {
        this.gameSession = gameSession;
        this.gameFrame = gameFrame;
    }

    @PostConstruct
    @Override
    public void init() {

    }
}
