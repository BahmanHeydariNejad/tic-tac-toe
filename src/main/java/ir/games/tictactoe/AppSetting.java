package ir.games.tictactoe;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.awt.*;

@Getter
@Setter
@Component
public class AppSetting {

    private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    @Value("${dooz.board.maxSize}")
    private Integer maxBoardSize;
    @Value("${dooz.board.turnTime}")
    private Integer turnTime;
    @Value("${dooz.board.minSize}")
    private Integer minBoardSize;
    @Value("plus.jpg")
    private Resource plusResource;
    @Value("minus.jpg")
    private Resource minusResource;

    @Value("square.jpg")
    private Resource squareResource;
    @Value("empty.jpg")
    private Resource emptyResource;

    public boolean isValidBoardSize(int boardSize) {
        return boardSize >= minBoardSize && boardSize <= maxBoardSize;
    }

}
