package ir.games.tictactoe.test;

import javax.swing.*;
import java.awt.*;

public class ScaledImageLabel extends JLabel {
    protected void paintComponent(Graphics g) {
        ImageIcon icon = (ImageIcon) getIcon();
        if (icon != null) {
            ImageDrawer.drawScaledImage(icon.getImage(), this, g);
        }
    }
}
