package ir.games.tictactoe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@Configuration
@EnableAsync
@EnableScheduling
@ConfigurationPropertiesScan(basePackages = "ir.*")
public class Start {
    private static final Logger LOGGER = LoggerFactory.getLogger(Start.class);

    private Environment environment;

    public Start(Environment environment) {
        this.environment = environment;
    }

    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(Start.class);
        builder.headless(false);
        ConfigurableApplicationContext context = builder.run(args);
    }

//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler(
//                "/webjars/**",
//                "/static/**"
//        ).addResourceLocations(
//                "classpath:/META-INF/resources/webjars/",
//                "classpath:/static/img/",
//                "classpath:/static/css/",
//                "classpath:/static/js/",
//                "classpath:/static/"
//        );
//    }
}
