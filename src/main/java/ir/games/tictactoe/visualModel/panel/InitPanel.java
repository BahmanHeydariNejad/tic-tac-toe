package ir.games.tictactoe.visualModel.panel;

import lombok.Getter;
import org.springframework.stereotype.Component;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

@Getter
@Component
public class InitPanel extends BasePanel {

    private JTextField playerName;
    private JSpinner boardSize;
    private JButton createButton;
    private JButton joinButton;
    private JTextArea tokenField;

    public InitPanel() {
        playerName = new JTextField();
        boardSize = new JSpinner();
        createButton = new JButton();
        joinButton = new JButton();
        tokenField = new JTextArea();
    }

    @Override
    public void init() {
        setBackground(Color.green);
        setLayout(new GridBagLayout());

        playerName.setText("Player Name");
        add(playerName, new GridBagConstraints(
                0, 0,
                3, 1,
                1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 20, 0, 2),
                0, 20)
        );

        SpinnerNumberModel spinnerModel = new SpinnerNumberModel();
        spinnerModel.setValue(5);
        spinnerModel.setMinimum(3);
        spinnerModel.setMaximum(20);
        boardSize.setModel(spinnerModel);
        add(boardSize, new GridBagConstraints(
                3, 0,
                1, 1,
                1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 2, 0, 20),
                0, 20)
        );

        createButton.setText("Create");
        add(createButton, new GridBagConstraints(
                0, 1,
                2, 1,
                1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 20, 0, 2),
                0, 5)
        );

        joinButton.setText("Join");
        add(joinButton, new GridBagConstraints(
                2, 1,
                2, 1,
                1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 2, 0, 20),
                0, 5)
        );

        tokenField.setWrapStyleWord(true);
        tokenField.setLineWrap(true);
        tokenField.setBorder(new TitledBorder("Server Token"));
        tokenField.setVisible(false);
        add(tokenField, new GridBagConstraints(
                0, 2,
                4, 1,
                1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 20, 0, 20),
                0, 100)
        );

    }
}
