package ir.games.tictactoe.visualModel.panel.panelcomponent;

import ir.games.tictactoe.logicalModel.Player;
import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;

@Getter
@Setter
public class PlayerPanel extends JPanel {

    private JLabel icon;
    private JTextField playerName;
    private JToggleButton readyCancelButton;

    public PlayerPanel(Player player) {
        setLayout(new GridBagLayout());

        add(new JLabel("Select Icon:"), new GridBagConstraints(
                0, 0,
                1, 1,
                1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0),
                0, 0)
        );

        add(new JLabel("Player Name:"), new GridBagConstraints(
                0, 1,
                1, 1,
                1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0),
                0, 0)
        );

        icon = new JLabel(player.getPieceType().name());
        add(icon, new GridBagConstraints(
                1, 0,
                2, 1,
                3, 1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 5, 0, 0),
                0, 0)
        );

        this.playerName = new JTextField(player.getDisplayName());
        add(this.playerName, new GridBagConstraints(
                1, 1,
                2, 1,
                3, 1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 5, 0, 0),
                5, 5)
        );

        readyCancelButton = new JToggleButton("Ready!");
        readyCancelButton.setBackground(Color.GREEN);
        readyCancelButton.addActionListener(e -> {
            if (readyCancelButton.isSelected()) {
                readyCancelButton.setText("Cancel");
                readyCancelButton.setBackground(Color.RED);
            } else {
                readyCancelButton.setText("Ready!");
                readyCancelButton.setBackground(Color.GREEN);
            }
        });
//        readyCancelButton.setEnabled(player.isHost());
        add(readyCancelButton, new GridBagConstraints(
                0, 2,
                3, 1,
                4, 1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0),
                0, 0)
        );

        setVisible(true);
    }
}
