package ir.games.tictactoe.visualModel.panel;

import ir.games.tictactoe.GameSession;
import ir.games.tictactoe.logicalModel.Player;
import ir.games.tictactoe.visualModel.panel.panelcomponent.BoardPanel;
import ir.games.tictactoe.visualModel.panel.panelcomponent.PlayerStatusPanel;
import lombok.Getter;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Component
public class GamePanel extends BasePanel {

    private Map<Player, PlayerStatusPanel> playerStatusPanels;
    private JProgressBar timeBar;
    private JProgressBar gameBar;

    private BoardPanel boardPanel;
    private GameSession gameSession;

    public GamePanel(GameSession gameSession) {
        this.gameSession = gameSession;

        timeBar = new JProgressBar();
        gameBar = new JProgressBar();
    }

    @Override
    public void init() {
        setLayout(new GridBagLayout());

        int boardSize = getGameSession().getMatch().getBoardSize();
        List<Player> players = getGameSession().getMatch().getPlayers();
        int insetSize = 10;

        playerStatusPanels = new HashMap<>();
        for (int i = 0; i < players.size(); i++) {
            PlayerStatusPanel temp = new PlayerStatusPanel(players.get(i));
            temp.setPlayer(players.get(i));
            temp.setBackground(Color.pink);
            add(temp, new GridBagConstraints(
                    0, i,
                    1, 1,
                    0, 0.1,
                    GridBagConstraints.CENTER, GridBagConstraints.NONE,
                    new Insets(insetSize, insetSize, insetSize, 0),
                    0, 0)
            );
            temp.setVisible(true);
            playerStatusPanels.put(players.get(i), temp);
        }

        boardPanel = new BoardPanel(getGameSession().getMatch().getBoard().getCells());
        boardPanel.setBackground(Color.gray);
        add(boardPanel, new GridBagConstraints(
                1, 0,
                5, players.size(),
                0.9, 0.9,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(insetSize, insetSize, insetSize, insetSize),
                0, 0)
        );

        timeBar.setOrientation(JProgressBar.VERTICAL);
        timeBar.setMinimum(0);
        timeBar.setMaximum(boardSize * boardSize);
        timeBar.setValue(0);
        timeBar.setStringPainted(true);
        timeBar.setString(timeBar.getValue() + "");
        timeBar.setBackground(Color.ORANGE);
        add(timeBar, new GridBagConstraints(
                6, 0,
                1, players.size(),
                0, 1,
                GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                new Insets(insetSize, 0, insetSize, insetSize),
                0, 0)
        );

        gameBar.setOrientation(JProgressBar.VERTICAL);
        gameBar.setMinimum(0);
        gameBar.setMaximum(10);
        gameBar.setValue(7);
        gameBar.setStringPainted(true);
        gameBar.setString(gameBar.getValue() + "");
        gameBar.setBackground(Color.YELLOW);
        add(gameBar, new GridBagConstraints(
                7, 0,
                1, players.size(),
                0, 1,
                GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                new Insets(insetSize, 0, insetSize, insetSize),
                0, 0)
        );
    }
}
