package ir.games.tictactoe.visualModel.panel.panelcomponent;

import ir.games.tictactoe.test.ImageDrawer;

import javax.swing.*;
import java.awt.*;

public class ResizableIcon extends JLabel {

    @Override
    protected void paintComponent(Graphics g) {
        ImageIcon icon = (ImageIcon) getIcon();
        if (icon != null) {
            ImageDrawer.drawScaledImage(icon.getImage(), this, g);
        }
    }

}
