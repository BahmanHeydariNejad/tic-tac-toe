package ir.games.tictactoe.visualModel.panel;

import ir.games.tictactoe.visualModel.BaseModule;

import javax.swing.*;

public abstract class BasePanel extends JPanel implements BaseModule {

    public BasePanel() {
        setVisible(false);
        this.setToolTipText(this.getClass().getSimpleName());
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible) {
            removeAll();
            init();
        }
        super.setVisible(visible);
    }
}
