package ir.games.tictactoe.visualModel.panel;

import lombok.Getter;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


@Getter
@Component
public class WaitingPanel extends BasePanel {

    private JLabel tokenLabel;
    private JLabel waitingText;
    private JButton cancelButton;

    public WaitingPanel() {
        waitingText = new JLabel();
        cancelButton = new JButton();
        tokenLabel = new JLabel();
    }

    @Override
    public void init() {
        setBackground(Color.orange);
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.anchor = GridBagConstraints.CENTER;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.gridx = 0;

        waitingText.setText("<html>Give following token to your opponent then wait for his/her respond…</html>");
        gbc.gridy = 0;
        add(waitingText, gbc);

        tokenLabel.setText("localhost:123456");
        Font font = new Font(getFont().getFontName(), Font.BOLD, 20);
        tokenLabel.setFont(font);
        tokenLabel.setForeground(Color.green);
        tokenLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(tokenLabel.getText()), null);
                JOptionPane.showMessageDialog(tokenLabel, "Token was copied to clipboard :)");
            }
        });
        gbc.gridy = 1;
        add(tokenLabel, gbc);

        cancelButton.setText("Cancel");
        gbc.gridy = 2;
        add(cancelButton, gbc);
    }
}
