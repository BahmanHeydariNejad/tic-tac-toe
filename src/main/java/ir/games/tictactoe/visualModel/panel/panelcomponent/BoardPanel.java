package ir.games.tictactoe.visualModel.panel.panelcomponent;

import ir.games.tictactoe.logicalModel.Cell;
import ir.games.tictactoe.visualModel.panel.BasePanel;

public class BoardPanel extends BasePanel {

    private BoardCell[][] boardCells;
    private Cell[][] cells;

    public BoardPanel(Cell[][] cells) {
        this.cells = cells;
    }

    @Override
    public void init() {
        boardCells = new BoardCell[cells.length][cells.length];

        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                boardCells[i][j] = new BoardCell(cells[i][j]);
            }
        }

    }
}
