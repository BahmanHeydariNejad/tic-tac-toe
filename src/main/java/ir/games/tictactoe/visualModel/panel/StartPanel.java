package ir.games.tictactoe.visualModel.panel;

import ir.games.tictactoe.GameSession;
import ir.games.tictactoe.logicalModel.Player;
import ir.games.tictactoe.visualModel.panel.panelcomponent.PlayerPanel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Component
public class StartPanel extends BasePanel {

    private List<PlayerPanel> playerPanels;
    private JLabel waitingText;
    private JLabel boardSize;
    private GameSession gameSession;

    public StartPanel(GameSession gameSession) {
        this.gameSession = gameSession;
        waitingText = new JLabel();
        boardSize = new JLabel();
    }

    public void init() {
        setLayout(new GridBagLayout());
        List<Player> players = getGameSession().getMatch().getPlayers();
        int boardSizeValue = getGameSession().getMatch().getBoardSize();

        playerPanels = new ArrayList<>();
        for (int i = 0; i < players.size(); i++) {
            PlayerPanel temp = new PlayerPanel(players.get(i));
            add(temp, new GridBagConstraints(
                    i, 1,
                    1, 1,
                    1, 4,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 20, 30, 20),
                    0, 0)
            );
        }

        boardSize.setText(String.valueOf(boardSizeValue));
        add(boardSize, new GridBagConstraints(
                0, 0,
                1, 1,
                1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0),
                0, 0)
        );

        waitingText.setText("Waiting for opponent(s) ...");
        add(waitingText, new GridBagConstraints(
                1, 0,
                players.size() - 1, 1,
                1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0),
                0, 0)
        );
    }
}
