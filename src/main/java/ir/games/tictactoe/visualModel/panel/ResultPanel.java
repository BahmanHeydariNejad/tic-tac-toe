package ir.games.tictactoe.visualModel.panel;

import ir.games.tictactoe.GameSession;
import ir.games.tictactoe.logicalModel.Player;
import ir.games.tictactoe.visualModel.panel.panelcomponent.PlayerStatusPanel;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.awt.*;

@Getter
@Component
public class ResultPanel extends BasePanel {

    private PlayerStatusPanel playerStatusPanel;
    private GameSession gameSession;

    public ResultPanel(GameSession gameSession) {
        this.gameSession = gameSession;
    }

    @Override
    public void init() {
        setBackground(Color.green);
        setLayout(new GridBagLayout());
        Player winner = getGameSession().getMatch().getMatchWinner();
        if (winner != null) {
            playerStatusPanel = new PlayerStatusPanel(gameSession.getMatch().getMatchWinner());
            add(playerStatusPanel, new GridBagConstraints(
                    0, 0,
                    1, 1,
                    0, 0,
                    GridBagConstraints.CENTER, GridBagConstraints.NONE,
                    new Insets(0, 0, 0, 0),
                    0, 0)
            );
        }
    }
}
