package ir.games.tictactoe.visualModel.panel.panelcomponent;

import ir.games.tictactoe.logicalModel.Player;
import ir.games.tictactoe.visualModel.panel.BasePanel;
import lombok.Getter;
import lombok.Setter;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class PlayerStatusPanel extends BasePanel {

    private JLabel playerName;
    private JLabel icon;

    @Getter
    @Setter
    private Player player;

    public PlayerStatusPanel(Player player) {
        this.player = player;
        this.playerName = new JLabel();
        this.icon = new JLabel();
    }

    @Override
    public void init() {
        setLayout(new GridBagLayout());

        this.playerName.setText(player.getDisplayName());
        add(this.playerName, new GridBagConstraints(
                0, 0,
                1, 1,
                1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0),
                0, 0)
        );

        try {
            this.icon.setIcon(new ImageIcon(ImageIO.read(getClass().getResourceAsStream(player.getPieceType().toString()))));
        } catch (IOException e) {
            e.printStackTrace();
        }
        add(this.icon, new GridBagConstraints(
                0, 1,
                1, 1,
                1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0),
                0, 0)
        );
    }

}
