package ir.games.tictactoe.visualModel.panel.panelcomponent;

import ir.games.tictactoe.logicalModel.Cell;
import ir.games.tictactoe.logicalModel.Player;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.IOException;
import java.net.URL;

public class BoardCell extends ResizableIcon {

    private Cell cell;

    public BoardCell(Cell cell) {
        this.cell = cell;
    }

    public void ownByPlayer(Player player) {
        this.cell.setPlayer(player);
        try {
            setIcon(new ImageIcon(ImageIO.read(new URL(cell.getPlayer().getPieceType().getPath()))));
            repaint();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}