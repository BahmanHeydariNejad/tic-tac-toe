package ir.games.tictactoe.visualModel.actions;

import ir.games.tictactoe.GameContext;
import org.springframework.stereotype.Component;

@Component
public class ResetActionHandler extends ActionHandler {

    public ResetActionHandler(GameContext gameContext) {
        super(gameContext);
    }

    @Override
    public void init() {

    }

}