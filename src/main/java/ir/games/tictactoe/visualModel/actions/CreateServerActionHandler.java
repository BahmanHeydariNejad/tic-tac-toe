package ir.games.tictactoe.visualModel.actions;

import ir.games.tictactoe.GameContext;
import org.springframework.stereotype.Component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Component
public class CreateServerActionHandler extends ActionHandler {

    private ActionListener createServer = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                getGameContext().getGameFrame().getInitPanel().getTokenField().setVisible(false);
                getGameContext().getGameFrame().getWaitingPanel().setVisible(true);
                getGameContext().getGameFrame().getInitPanel().setVisible(false);

                System.out.println("Creating to server ...");
            } catch (Exception ex) {
                ex.printStackTrace();
                getGameContext().getGameFrame().getInitPanel().setVisible(true);
                getGameContext().getGameFrame().getWaitingPanel().setVisible(false);
            }
        }
    };

    public CreateServerActionHandler(GameContext gameContext) {
        super(gameContext);
    }

    @Override
    public void init() {
        getGameContext().getGameFrame().getInitPanel().getCreateButton().addActionListener(createServer);
    }
}