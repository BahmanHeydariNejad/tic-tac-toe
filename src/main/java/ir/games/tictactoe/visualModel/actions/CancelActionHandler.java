package ir.games.tictactoe.visualModel.actions;

import ir.games.tictactoe.GameContext;
import org.springframework.stereotype.Component;

@Component
public class CancelActionHandler extends ActionHandler {

    public CancelActionHandler(GameContext gameContext) {
        super(gameContext);
    }

    @Override
    public void init() {
        System.out.println("AAA");
        getGameContext().getGameFrame().getWaitingPanel().getCancelButton().addActionListener(e -> {
            getGameContext().getGameFrame().getInitPanel().setVisible(true);
            getGameContext().getGameFrame().getWaitingPanel().setVisible(false);
        });
    }
}