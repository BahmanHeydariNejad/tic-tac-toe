package ir.games.tictactoe.visualModel.actions;

import ir.games.tictactoe.GameContext;
import org.springframework.stereotype.Component;

@Component
public class CheckWinActionHandler extends ActionHandler {

    public CheckWinActionHandler(GameContext gameContext) {
        super(gameContext);
    }

    @Override
    public void init() {
        getGameContext().getGameFrame().getWaitingPanel().getCancelButton().addActionListener(e -> {
            System.out.println(this);
        });
    }
}