package ir.games.tictactoe.visualModel.actions;

import ir.games.tictactoe.GameContext;
import ir.games.tictactoe.visualModel.BaseModule;
import lombok.Getter;

public abstract class ActionHandler implements BaseModule {

    @Getter
    protected GameContext gameContext;

    public ActionHandler(GameContext gameContext) {
        this.gameContext = gameContext;
        System.out.println(this);
    }
}
