package ir.games.tictactoe.visualModel.actions;

import ir.games.tictactoe.GameContext;
import org.springframework.stereotype.Component;

@Component
public class ReadyActionHandler extends ActionHandler {

    public ReadyActionHandler(GameContext gameContext) {
        super(gameContext);
    }

    @Override
    public void init() {

    }

}