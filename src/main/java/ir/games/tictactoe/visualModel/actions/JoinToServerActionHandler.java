package ir.games.tictactoe.visualModel.actions;

import ir.games.tictactoe.GameContext;
import org.springframework.stereotype.Component;

import java.awt.event.ActionListener;

@Component
public class JoinToServerActionHandler extends ActionHandler {

    private ActionListener joinToServer = e -> {
        boolean visible = getGameContext().getGameFrame().getInitPanel().getTokenField().isVisible();
        if (visible) {
            connectToServer();
        } else {
            getGameContext().getGameFrame().getInitPanel().setVisible(true);
            getGameContext().getGameFrame().getInitPanel().getTokenField().setVisible(true);
        }
    };

    public JoinToServerActionHandler(GameContext gameContext) {
        super(gameContext);
    }

    @Override
    public void init() {
        getGameContext().getGameFrame().getInitPanel().getJoinButton().addActionListener(joinToServer);
    }

    private void connectToServer() {
        try {
            getGameContext().getGameFrame().getWaitingPanel().setVisible(true);
            getGameContext().getGameFrame().getInitPanel().setVisible(false);

        } catch (Exception e) {
            getGameContext().getGameFrame().getInitPanel().setVisible(true);
            getGameContext().getGameFrame().getWaitingPanel().setVisible(false);
        }
        System.out.println("Connecting ...");
    }

}