package ir.games.tictactoe.visualModel.actions;

import ir.games.tictactoe.GameContext;
import org.springframework.stereotype.Component;

@Component
public class DoMoveActionHandler extends ActionHandler {

    public DoMoveActionHandler(GameContext gameContext) {
        super(gameContext);
    }

    @Override
    public void init() {

    }

}