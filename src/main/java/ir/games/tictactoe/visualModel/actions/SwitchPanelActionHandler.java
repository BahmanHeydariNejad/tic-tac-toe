package ir.games.tictactoe.visualModel.actions;

import ir.games.tictactoe.GameContext;
import org.springframework.stereotype.Component;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

@Component
public class SwitchPanelActionHandler extends ActionHandler {

    private MouseWheelListener mouseWheelListener = new MouseWheelListener() {
        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            e.consume();

            java.awt.Component[] components = getGameContext().getGameFrame().getBasePanel().getComponents();
            if (e.isControlDown() && e.getPreciseWheelRotation() > 0) {
                for (int i = 0; i < components.length; i++) {
                    if (components[i].isVisible()) {
                        java.awt.Component component = i + 1 < components.length ? components[i + 1] : components[0];
                        component.setVisible(true);
                        components[i].setVisible(false);
                        break;
                    }
                }
            } else if (e.isControlDown() && e.getPreciseWheelRotation() < 0) {
                for (int i = components.length - 1; i >= 0; i--) {
                    if (components[i].isVisible()) {
                        java.awt.Component component = i - 1 > 0 ? components[i - 1] : components[components.length - 1];
                        component.setVisible(true);
                        components[i].setVisible(false);
                        break;
                    }
                }

            }
        }
    };

    public SwitchPanelActionHandler(GameContext gameContext) {
        super(gameContext);
    }

    @Override
    public void init() {
        getGameContext().getGameFrame().getBasePanel().addMouseWheelListener(mouseWheelListener);
    }
}