package ir.games.tictactoe.visualModel;

import ir.games.tictactoe.visualModel.panel.*;
import lombok.Getter;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

@Getter
@org.springframework.stereotype.Component
public class GameFrame extends JFrame implements BaseModule {

    private JPanel basePanel;

    private InitPanel initPanel;
    private WaitingPanel waitingPanel;
    private StartPanel startPanel;
    private GamePanel gamePanel;
    private ResultPanel resultPanel;

    private JLabel message;

    public GameFrame(InitPanel initPanel, WaitingPanel waitingPanel, StartPanel startPanel, GamePanel gamePanel, ResultPanel resultPanel) throws HeadlessException {
        this.initPanel = initPanel;
        this.waitingPanel = waitingPanel;
        this.startPanel = startPanel;
        this.gamePanel = gamePanel;
        this.resultPanel = resultPanel;
    }

    @PostConstruct
    @Override
    public void init() {
        setContentPane(new JPanel());
        getContentPane().setLayout(new GridBagLayout());
        getContentPane().setBackground(Color.yellow);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setMinimumSize(new Dimension(560, 350));
        setSize(getMinimumSize());
        setPreferredSize(getSize());
        setMaximumSize(screenSize);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        basePanel = new JPanel();
        basePanel.setLayout(new CardLayout());
        getContentPane().add(basePanel, new GridBagConstraints(
                0, 0,
                1, 1,
                1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0),
                0, 0)
        );

        message = new JLabel("Status ...");
        getContentPane().add(message, new GridBagConstraints(
                0, 1,
                1, 1,
                0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0),
                0, 0)
        );

        initPanel.setVisible(true);
        basePanel.add(initPanel);
        basePanel.add(waitingPanel);
        basePanel.add(startPanel);
        basePanel.add(gamePanel);
        basePanel.add(resultPanel);

        pack();
        setVisible(true);
    }

}
